package org.opensource.kojast.data.prefs

import android.content.Context
import android.content.SharedPreferences

class StorageHelper(context: Context) {

    private val prefName = "KOJAST"

    private val settings: SharedPreferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE)

    fun setString(key: String, value: String) = settings.edit().putString(key, value).apply()

    fun setInt(key: String, value: Int) = settings.edit().putInt(key, value).apply()

    fun setBool(key: String, value: Boolean) = settings.edit().putBoolean(key, value).apply()


    fun getString(key: String, defValue: String) = settings.getString(key, defValue)

    fun getInt(key: String, defValue: Int) = settings.getInt(key, defValue)

    fun getBool(key: String, defValue: Boolean) = settings.getBoolean(key, defValue)


}