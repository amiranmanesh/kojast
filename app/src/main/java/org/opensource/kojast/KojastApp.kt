package org.opensource.kojast

import android.support.multidex.MultiDexApplication
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import io.reactivex.plugins.RxJavaPlugins
import ir.malv.utils.Pulp
import org.opensource.kojast.utils.Constant


class KojastApp : MultiDexApplication() {

    companion object {
        const val TAG = "Application"
    }

    override fun onCreate() {
        super.onCreate()
        Pulp.init(this).setMainTag(Constant.LOG_TAG)
        //instance = this
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/font_light.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )
        Thread.setDefaultUncaughtExceptionHandler { t, e ->
            Pulp.wtf(TAG, "** Uncaught exception occurred **\n Thread: ${t.name}\n Cause: ${e.message}", e)
        }
        RxJavaPlugins.setErrorHandler { throwable ->
            Pulp.wtf(TAG, "** Uncaught exception occurred **\n Cause: ${throwable.message}", throwable)
        }
    }
}