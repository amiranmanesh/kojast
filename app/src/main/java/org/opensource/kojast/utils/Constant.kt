package org.opensource.kojast.utils

object Constant {

    // Debugging log tag
    const val LOG_TAG = "OpenSource_Kojast"
}