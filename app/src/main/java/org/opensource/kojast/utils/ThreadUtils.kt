package org.opensource.kojast.utils

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object ThreadUtils {

    fun <T> Observable<T>.async() = observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
    fun <T> Flowable<T>.async() = observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
    fun <T> Single<T>.async() = observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
    fun Completable.async() = observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
}