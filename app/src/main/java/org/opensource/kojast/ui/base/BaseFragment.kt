package org.opensource.kojast.ui.base

import android.content.Context
import android.support.v4.app.Fragment
import android.view.View
import android.widget.Toast
import ir.malv.utils.Pulp

abstract class BaseFragment : Fragment() {

    protected lateinit var contexts: Context
    protected lateinit var views: View

    companion object {
        var TAG = "BaseFragment"
    }

    /**
     * Logging into fireBase analytics for each activity
     */
    override fun onResume() {
        super.onResume()
        log(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        log(TAG, "onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        log(TAG, "onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        log(TAG, "onDetach")
    }
    //End


    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun toast(message: String) {
        Toast.makeText(contexts, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun longToast(message: String) {
        Toast.makeText(contexts, message, Toast.LENGTH_LONG).show()
    }

    /**
     * To log an event to analytics.
     */
    private fun log(screenName: String, event: String) {
        Pulp.info(BaseActivity.TAG, "$screenName -> $event")
    }
}