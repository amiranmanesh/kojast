package org.opensource.kojast.ui.base

import io.reactivex.disposables.CompositeDisposable
import ir.malv.utils.Pulp

abstract class BasePresenter {

    protected val disposable = CompositeDisposable()

    /**
     * Close network stream
     */
    private fun closeNetwork() {
        disposable.dispose()
    }


    /**
     * Close streams when ui destroyed
     */
    fun onDestroy() {
        try {
            closeNetwork()
        } catch (e: Exception) {
            Pulp.wtf("BasePresenter", "Couldn't close stream.", e)
        }

    }
}