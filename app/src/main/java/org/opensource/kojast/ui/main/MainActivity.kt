package org.opensource.kojast.ui.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import org.opensource.kojast.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
