package org.opensource.kojast.ui.main

import android.app.Activity
import android.content.Context
import android.preference.PreferenceManager
import org.opensource.kojast.BuildConfig
import org.opensource.kojast.ui.base.BasePresenter
import org.osmdroid.config.Configuration

class MainPresenter(private val activity: Activity, private val context: Context, private val view: View) :
    BasePresenter() {

    init {
        Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context))
        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID
    }

    /**
     * This is the connector of activity and presenter.
     * This can be a separate interface file, but eh.
     * Each time view needs to be updated presenter uses these functions to log data ro Ui activity or fragment
     */
    interface View {

    }
}