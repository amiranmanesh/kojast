package org.opensource.kojast.ui.base

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import ir.malv.utils.Pulp
import kotlin.system.exitProcess


@Suppress("DEPRECATION")
abstract class BaseActivity : AppCompatActivity() {

    companion object {
        var TAG = "BaseActivity"
    }

    protected lateinit var fragmentManager: FragmentManager

    /**
     *
     * To use and configure font
     * Calligraphy used to configure font.
     */
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    /**
     * If want to add some features to current toolbar
     * we will get it using this function
     *
     * @return the default toolbar
     */
//    protected val defaultToolbar: Toolbar
//        get() = findViewById<View>(R.id.toolbar) as Toolbar

    /**
     * Logging into fireBase analytics for each activity
     */
    //Begin
    override fun onRestart() {
        super.onRestart()
        log(this.localClassName, "onRestart")
    }

    override fun onResume() {
        super.onResume()
        log(this.localClassName, "onResume")
    }

    override fun onPause() {
        super.onPause()
        log(this.localClassName, "onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        log(this.localClassName, "onDestroy")
    }
    //End

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun setStatusBarGradiant(activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            val window = activity.window
//            val background = activity.resources.getDrawable(R.drawable.gradient_full_back)
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//            window.statusBarColor = activity.resources.getColor(android.R.color.transparent)
//            window.navigationBarColor = activity.resources.getColor(android.R.color.black)
//            window.setBackgroundDrawable(background)
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Set orientation to Portrait
        log(this.localClassName, "onCreate")
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
//            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        }

        // Handle uncaught exceptions.
        Thread.setDefaultUncaughtExceptionHandler { paramThread, paramThrowable ->
            // If any exception was unhandled this will be logged
            // Since all activities extend this class, it is possible to see all errors.
            Pulp.wtf(
                TAG,
                "Unhandled exception occurred.\n" +
                        "Thread: " + paramThread.name,
                Exception(paramThrowable)
            )
            paramThrowable.printStackTrace()
            // Without this command this code does not work.
            exitProcess(2)
        }

        supportActionBar?.hide()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            setStatusBarGradiant(this)
        }

        fragmentManager = supportFragmentManager


    }

    fun isShowFragment(tag: String): Boolean {
        val fragment = fragmentManager.findFragmentByTag(tag)
        return fragment != null
    }

    fun hideFragment(tag: String) {
        val fragment = fragmentManager.findFragmentByTag(tag)
        val ft = fragmentManager.beginTransaction()
        fragment?.let { ft.hide(it) }
        ft.commit()
    }

    fun showFragment(tag: String) {
        val fragment = fragmentManager.findFragmentByTag(tag)
        val ft = fragmentManager.beginTransaction()
        fragment?.let { ft.show(it) }
        ft.commit()
    }


    /**
     * Get Called when we want to hide softKeyboard
     */
    protected fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun longToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    /**
     * Show a simple dialog
     */
    protected fun showSimpleDialog(title: String, message: String) {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("خب", null)
            .create()
            .show()
    }

    /**
     * To log an event
     */
    private fun log(screenName: String, event: String) {
        Pulp.info(TAG, "$screenName -> $event")
    }
}